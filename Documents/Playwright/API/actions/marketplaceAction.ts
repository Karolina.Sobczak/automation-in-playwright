import {APIResponse} from '@playwright/test';
import {MarketplaceController} from "../controllers/marketplaceController";

export class MarketplaceAction {
    readonly marketplaceController = new MarketplaceController();

    async getListingServices(customerId: string, listingId: string ): Promise<APIResponse>  {
        return this.marketplaceController.getListingServices(customerId, listingId);
    }
}