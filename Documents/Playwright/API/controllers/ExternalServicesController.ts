import { chromium } from 'playwright';
import ExternalServicesAction from '../Controllers/LoginController';
import NewProspectModelForAPI from '../../Models/NewProspectModelForAPI';

class ExternalServicesController {
    private readonly auroraUrl: string;
    private readonly externalServicesObj: ExternalServicesAction;
    private newProspectModel: NewProspectModelForAPI;

    constructor() {
        this.auroraUrl = process.env.auroraUrl; // Assuming auroraUrl is set in environment variables
        this.externalServicesObj = new ExternalServicesAction();
        this.newProspectModel = new NewProspectModelForAPI();
    }

    async createNewProspectAPI(authCode: string): Promise<void> {
        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        await page.route('**/ExternalServices/v1/Accounts/Leads/Create', route => {
            route.request().continue({
                headers: {
                    ...route.request().headers(),
                    Authorization: `Basic ${authCode}`
                }
            });
        });

        await page.goto(`${this.auroraUrl}/ExternalServices/v1/Accounts/Leads/Create`, {
            method: 'POST',
            headers: {
                Authorization: `Basic ${authCode}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                first_name: this.newProspectModel.first_name,
                last_name: this.newProspectModel.last_name,
                email: this.newProspectModel.email,
                name: this.newProspectModel.name,
                account_name: this.newProspectModel.account_name
            })
        });

        await browser.close();
    }
}

export default ExternalServicesController;
