import { test } from '@playwright/test';
import SessionHelper from '../../Helpers/SessionHelperVFDirect';
import NewSalePage from '../../Pages/NewSales/NewSalePage';
import OrderMobilePage from '../../Pages/NewSales/OrderMobilePage';
import ProductCustomisePage from '../../Pages/NewSales/ProductCustomisePage';
import ProductDeliveryPage from '../../Pages/NewSales/ProductDeliveryPage';
import ProductSummaryPage from '../../Pages/NewSales/ProductSummaryPage';

test.describe('Login tests + selecting Active Customer', () => {
    const sessionHelperObj = new SessionHelper();
    const newSalePageObj = new NewSalePage();
    const orderMobilePageObj = new OrderMobilePage();
    const productCustomisePageObj = new ProductCustomisePage();
    const productDeliveryPageObj = new ProductDeliveryPage();
    const productSummaryPageObj = new ProductSummaryPage();

    test.beforeEach(async () => {
        await sessionHelperObj.loginAsStandardUserAndStoreContext();
        await newSalePageObj.navigate();
    });

    test('Order Mobile product', async () => {
        await newSalePageObj.createNewProspectForExisitingCustomer('CyressTest1Mobile');

        await orderMobilePageObj.completeOrderMobileVFDirect(
            '12', // noMonths
            'Business Advance Extra 1 SIMO (BUAE01VSR)', // productName
            '1' // quantityProduct
        );

        await productCustomisePageObj.completeOrderMobileCustomisePageVFDirect();
        await productDeliveryPageObj.completeOrderDeliveryPageVFDirect();
        await productSummaryPageObj.completeProductSummaryPageVFDirect();
    });
});
