import testConfig from "../../testConfig";
import {APIResponse} from '@playwright/test';
import {BaseApiContext} from "@componentLibrary/baseApiContext";

export class MarketplaceController {
    readonly baseUrl = testConfig.qaApi;
    readonly marketplacePath = "Marketplace/v1/api/Marketplace/";
    readonly context;

    constructor() {
        this.context = BaseApiContext.getContext(BaseApiContext.ContextKey.Default);        
    }

    async getListingServices(customerId: string, listingId: string ): Promise<APIResponse> {
        
        return this.context.get(
            `${this.baseUrl}${this.marketplacePath}customers/${customerId}/listings/${listingId}/listing-services`);
    }
}


