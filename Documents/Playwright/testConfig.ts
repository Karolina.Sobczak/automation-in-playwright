const testConfig = {
    baseUrl: `https://uat.sales.affinity.io/`,
    qaApi: `https://preprod-services-api-management.azure-api.net/`,
    affinitySales: {
        baseUrl: `https://uat.sales.affinity.io/`,
        environment: '08',
        Username: `andrew.lovetesting`,
        Password: `CYq9aZ2u`,
    },
    VFDirect:{
        baseUrl: 'https://vodafone.uat.sales.affinity.io/',
        environment: '08',
        Username: `VFDirect`,
        Password: `VFDirect1`,
    },
    engineerUsername: `eng111.samsonitservices`,
    engineerPassword: `testPassw#rd2`,
    customerAdminUsername: `adminuser@john.com`,
    customerAdminPassword: `testPassw#rd2`,
    customerWithLegacyMicrosoft: `18032024a`,
    defaultCustomerFirstname: "Bill",
    defaultCustomerSurname: "Muller",
    waitForElement: 120000,
    dbUsername: ``,
    dbPassword: ``,
    dbServerName: ``,
    dbPort: ``,
    dbName: ``
}
{
  "compilerOptions": {
    "target": "ES6",
    "module": "commonjs",
    "strict": true,
    "esModuleInterop": true,
    "skipLibCheck": true,
    "forceConsistentCasingInFileNames": true,
    "outDir": "./dist",
    "rootDir": "./src"
  },
  "include": ["src/**/*.ts"],
  "exclude": ["node_modules"]
}

 
export default testConfig;