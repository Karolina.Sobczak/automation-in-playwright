// import { BasePage } from "@pages/basePage";
import { Page, BrowserContext, Locator } from '@playwright/test';

export class NewSalePage {
    // page, context
  readonly page: Page;
 // readonly basePage: BasePage;
 // readonly context: BrowserContext;
  readonly newSaleOnExistingCustomerButton: Locator;
  readonly newSaleButton: Locator;
  readonly newOpportunityNameInput: Locator;
  readonly newOpportunityCreateSaleButton: Locator;


  constructor(page: Page) {
    // page, context
    this.page = page;
   // this.basePage = basePage;
    //this.context = context;
    this.newSaleButton = page.getByText('New Sale');
    this.newSaleOnExistingCustomerButton = page.locator("//*[@id='new-sale-button-13a8d5a2-4c0a-94bc-295b-60001dc629b5']");
    this.newOpportunityNameInput = page.locator('//*[@name="orderName"]'); 
    this.newOpportunityCreateSaleButton = page.getByRole("button", { name: 'Create'});

  }
   /* async navigate() {
        await this.page.goto('https://uat.sales.affinity.io/sales/new');
    }
*/  

    async clickNewSale() {

   // await this.page.waitForLoadState('domcontentloaded');
    await this.newSaleButton.click();

    }

    async clickFirstNewSaleButtonOnExistingCustomer() {

       // await this.page.waitForLoadState('domcontentloaded');
        await this.newSaleOnExistingCustomerButton.click();
    }

    async enterNewOpportunityName(name: string) {

        await this.newOpportunityNameInput.fill('CyressTest1Mobile');
    }

    async clickNewOpportunityCreateSaleButton() {

        await this.newOpportunityCreateSaleButton.click();
    }

    async createNewProspectForExistingCustomer() {
        await this.clickNewSale();
        await this.clickFirstNewSaleButtonOnExistingCustomer();
        await this.enterNewOpportunityName('CyressTest1Mobile');
        await this.clickNewOpportunityCreateSaleButton();
    }
}

export default NewSalePage;
