import loginDetails from '../fixtures/loginDetails'
import { expect } from '@playwright/test';
import test from '../componentsLibrary/baseTest';
import { test as baseTest } from '@playwright/test';

    test.beforeEach(async ({page, loginPage}) => {
       // await loginPage.goToPage();
       await page.goto('https://uat.sales.affinity.io/');
    });

    test('Valid login credentials', async ({loginPage}) => {
        await loginPage.selectEnv(loginDetails.loginDet.environmentNumber);
        await loginPage.enterUsername(loginDetails.loginDet.username);
        await loginPage.enterPassword(loginDetails.loginDet.password);
        
      //  await loginPage.loginAsDefaultUser();
      //  await loginPage.ensureImLoggedIn();
        await loginPage.clicklogInButton();

        await expect(loginPage.page).toHaveURL('https://uat.sales.affinity.io/sales');
    });

    test('Invalid login username', async ({loginPage}) => {
        await loginPage.selectEnv(loginDetails.loginDet.environmentNumber);
        await loginPage.enterUsername('badUsername');
        await loginPage.enterPassword(loginDetails.loginDet.password);
        await loginPage.clicklogInButton();

    
        await expect(loginPage.logInError).toBeVisible();
        
    });

    test('Invalid login password', async ({loginPage}) => {
        await loginPage.selectEnv(loginDetails.loginDet.environmentNumber);
        await loginPage.enterUsername(loginDetails.loginDet.username);
        await loginPage.enterPassword('badPassword');
        await loginPage.clicklogInButton();

      //  await expect(loginPage.page).toHaveURL('*/');
        await expect(loginPage.logInError).toBeVisible();
    });
