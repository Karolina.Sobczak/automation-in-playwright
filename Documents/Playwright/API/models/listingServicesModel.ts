export interface ListingServices {
  headline: Headline
  services: Service[]
}

export interface Headline {
  description: string
  header: string
}

export interface Service {
  name: string
  price: number
  description: string
  currencyCode: string
}