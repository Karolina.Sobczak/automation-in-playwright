import faker from 'faker';
import { NewProspectModel } from '../Models/NewProspectModel';

class FakerHelper {

    createRandomUser(): NewProspectModel {
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        
        return {
            ContactFirstName: firstName,
            ContactLastName: lastName,
            Email: faker.internet.email({ firstName, lastName }),
            CompanyName: faker.company.companyName(),
            AddAnOpportunityName: faker.company.catchPhrase()
        };
    }
}

export default FakerHelper;
