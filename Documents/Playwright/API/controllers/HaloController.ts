import { chromium } from 'playwright';

class HaloController {
    private haloUrl: string;

    constructor() {
        this.haloUrl = process.env.haloUrl; // Assuming haloUrl is set in environment variables
    }

    async login(authCode: string): Promise<void> {
        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        await page.goto(`${this.haloUrl}/auth/Account/Login?returnurl=%2Fauth%2Fauthorize%3Fclient_id%3D34fe0a24-85d5-46d4-b9c6-721e23f25843%26response_type%3Dcode%26scope%3Dall%26redirect_uri%3Dhttps%253a%252f%252fgiacom.haloitsm.com%252fportal%252fauth%26act_as%3Dnull%26code_challenge%3DVqbuy_IzZdkwBetBe8fhN-lNYs2YrUv0nQTCVnagSNo%26code_challenge_method%3DS256%26state%3DnhWTVcqrCfQ14uyVWbhqFPwcQyh40SftScEbTjHGU9I%26nonce%3DfHZr3V_WbJUQxXO2LXDjn01_fa4wDXceb4irlR_jjGU`);
        
        await page.waitForNavigation(); // Wait for navigation to complete

        await page.close();
        await browser.close();
    }

    async token(authCode: string): Promise<void> {
        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        await page.goto(`${this.haloUrl}/auth/token?Redirect_Uri=https%3A%2F%2Fgiacom.haloitsm.com%2Fportal%2Fauth`);
        
        await page.waitForNavigation(); // Wait for navigation to complete

        await page.close();
        await browser.close();
    }
}

export default HaloController;
