//import { expect } from '@playwright/test';
import { NewSalePage } from '../Pages/NewSalePage';
import { OrderMobilePage } from '../Pages/OrderMobilePage';
//import ProductCustomisePage from '../Pages/ProductCustomisePage';
//import ProductDeliveryPage from '../Pages/ProductDeliveryPage';
import { ProductSummaryPage } from '../Pages/ProductSummaryPage';
import loginDetails from 'fixtures/loginDetails';
import test from '../componentsLibrary/baseTest';
import { LoginPage } from 'Pages/LoginPage';

/* test.beforeEach(async ({page, loginPage}) => {
    loginPage.loginAsDefaultUser();
   // loginPage.ensureImLoggedIn();
 THERE SHOOULDN'T BE ANY BEFOReACH BECAUSE THERE IS MORE THAN 1 TEST CASE and we dont need to login before every each
 }); */
     
    test.describe.configure({ mode: 'serial' });
    
  /*  test.beforeAll(async function () {
      /*global.browser = await chromium.launch({
              headless: false,
          });
          global.page = await global.browser.newPage();
          await this.page.goto('https://mypage.com');
         // ***some authentication steps *** 
          await this.page.context().storageState({ path: 'storageState.json' });
        });
      */
    test('Login + selecting Active Customer, selecting a product, customise the product', async ({loginPage, orderMobilePage, newSalePage,productCustomisePage, productDeliveryPage}) => {
    
        test.setTimeout(30000);
        await loginPage.loginAsDefaultUser();
        await newSalePage.createNewProspectForExistingCustomer();
        await orderMobilePage.completeOrderMobileWithData();

    

     // test.use({ storageState: 'storageState/auth_user.json' }); DOESNT WORK
      // 'storageState/auth_user.json' authenticated-state.json'
      
      // productSummaryPage
        test.setTimeout(100000);
        await productCustomisePage.completeOrderMobileCustomisePage();
        await productDeliveryPage.completeDeliveryWithData();
      //  await productSummaryPage.completeProductSummaryPage();
    });

