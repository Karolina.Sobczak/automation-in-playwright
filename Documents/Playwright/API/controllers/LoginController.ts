import { chromium } from 'playwright';

export class LoginController {
    private readonly auroraUrl: string;

    constructor() {
        this.auroraUrl = process.env.auroraUrl; // Assuming auroraUrl is set in environment variables
    }

    async login(permissionNumber: string, authCode: string): Promise<void> {
        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        await page.goto(`${this.auroraUrl}/api/v2/login?include_permissions=${permissionNumber}`, {
            method: 'POST',
            headers: {
                Authorization: authCode,
                'Content-Type': 'application/json'
            }
        });

        await page.waitForNavigation(); // Wait for navigation to complete

        await page.close();
        await browser.close();
    }

    async jwt(authCode: string): Promise<void> {
        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        await page.goto(`${this.auroraUrl}/api/v2/jwt`, {
            method: 'POST',
            headers: {
                Authorization: authCode,
                'Content-Type': 'application/json'
            }
        });

        await page.waitForNavigation(); // Wait for navigation to complete

        await page.close();
        await browser.close();
    }
}

export default LoginController;
