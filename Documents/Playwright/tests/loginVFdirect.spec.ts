import { test, expect } from '@playwright/test';
import LoginPageVFDirect from '../../Pages/LoginPageVFDirect';
import { LoginDetailsModel } from '../../Models/LoginDetailsModel';

test.describe('Login to VFDirect Platform', () => {
    let loginTestModelObj: LoginDetailsModel;
    const LoginPageVFobj = new LoginPageVFDirect();

    test.beforeEach(async () => {
        loginTestModelObj = await import('../../fixtures/loginDetailsVFDirect.json') as LoginDetailsModel;
        await LoginPageVFobj.navigate();
    });

    test('Valid login credentials', async ({ page }) => {
        await LoginPageVFobj.selectEnv(loginTestModelObj.environmentNumber);
        await LoginPageVFobj.enterUsername(loginTestModelObj.username);
        await LoginPageVFobj.enterPassword(loginTestModelObj.password);
        await LoginPageVFobj.clicklogInButton();

        expect(page.url()).toContain('/sales');
    });
});
