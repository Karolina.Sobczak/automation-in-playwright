import { Page } from 'playwright';
import ProductDeliveryPage from '../Pages/ProductDeliveryPage';
import { expect } from '@playwright/test';

export class ProductSummaryPage {
    page: Page;
    productDeliveryPage: ProductDeliveryPage;

    constructor(page: Page) {
        this.page = page;
        this.productDeliveryPage = new ProductDeliveryPage(page);
    }

    async callInterceptSaveWizardState() {
        await this.page.waitForResponse('**/SaveWizardState/*', { timeout: 90000 });
    }

    async clickApprovedButton() {
        await this.page.click('[class="MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedInherit MuiButton-sizeMedium MuiButton-outlinedSizeMedium MuiButton-colorInherit MuiButton-root MuiButton-outlined MuiButton-outlinedInherit MuiButton-sizeMedium MuiButton-outlinedSizeMedium MuiButton-colorInherit css-4exwe7"]:first');
    }

    async comparingSummaryPagePrice(deliveryPrice: string) {
        const summaryPagePrice = await this.page.$eval('[data-cy="price-Per-Month"]', el => el.textContent.trim());
        expect(summaryPagePrice).toBe(deliveryPrice);
    }

    async completeProductSummaryPageVFDirect() {
        await this.productDeliveryPage.storingDeliveryPagePrice();
        const deliveryPrice = await this.page.getAttribute('@deliveryPrice', 'textContent');
        await this.comparingSummaryPagePrice(deliveryPrice);
    }

    async completeProductSummaryPage() {
        await this.callInterceptSaveWizardState();
        await this.productDeliveryPage.storingDeliveryPagePrice();
        const deliveryPrice = await this.page.getAttribute('@deliveryPrice', 'textContent');
    }}
    
