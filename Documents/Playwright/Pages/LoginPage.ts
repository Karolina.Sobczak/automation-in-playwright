import { Page, BrowserContext, Locator, expect } from "@playwright/test";
import loginDetails from "fixtures/loginDetails";
import testConfig from "testConfig";

export class LoginPage {
    context: BrowserContext
    readonly logInButton: Locator;
    readonly logInError: Locator;
    readonly page: Page;
    readonly usernameInput: Locator;
    readonly passInput: Locator;
    readonly loginButton: Locator;  
    readonly envDropDown: Locator;

    constructor(page: Page, context: BrowserContext ) {
        this.context = context
        this.page = page;
        this.usernameInput = page.locator('[name="username"]');
        this.passInput = page.locator('[name="password"]');
        this.loginButton = page.locator('text="Log in"');
        this.logInError = page.getByText('Sign in error');
        this.envDropDown = page.locator('//div[contains(text(), "https://extusers.uat.aurora.io/api/v2/")]');
        
    }

     async goToPage() {
        await this.page.goto('https://uat.sales.affinity.io/');
    }
    
   async loginPage() {
    await this.loginAsDefaultUser();

   }


    async envButton(environmentNumber: string): Promise<Locator>{
        return this.page.getByRole("option", {name:'https://pub-external-users-testenv' + environmentNumber + '.uat.aurora.io/api/v2/'});
    }

    

    async selectEnv(environmentNumber: string) {
        await this.page.waitForLoadState('domcontentloaded');
        await this.envDropDown.click();
        const envButton = await this.envButton(environmentNumber);
        await envButton.click();
    
    }


    async enterUsername(username: string) {
        await this.usernameInput.click();
        await this.usernameInput.fill(username);
    }

    async enterPassword(password: string) {
        await this.passInput.click();
        await this.passInput.fill(password);
    }    

    async clicklogInButton() {
        await this.page.waitForLoadState('domcontentloaded');
        await this.loginButton.click();
    }

    async LogInto(environmentNumber: string, username: string, password: string) {
        console.log(environmentNumber, username, password);
        await this.goToPage();
        await this.selectEnv(loginDetails.loginDet.environmentNumber);
        await this.enterUsername(loginDetails.loginDet.username);
        await this.enterPassword(loginDetails.loginDet.password);
        await this.clicklogInButton();
        
        await expect(this.page).toHaveURL(/.*sales/) 
    }

    async loginAsDefaultUser(): Promise<void>{
      this.LogInto(loginDetails.loginDet.environmentNumber, loginDetails.loginDet.username, loginDetails.loginDet.password)
    }

   /* async ensureImLoggedIn(){
        await this.page.goto(`${testConfig.baseUrl}`);
        await this.page.waitForLoadState('domcontentloaded');
        if(this.page.url().includes('sales') == false) {
            this.loginAsDefaultUser();
            await this.context.storageState({path: 'storageState/auth_user.json'})
        }
            */
    }
//}



