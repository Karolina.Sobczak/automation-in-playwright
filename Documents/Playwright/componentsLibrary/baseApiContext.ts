import {request, APIRequestContext} from '@playwright/test';

export abstract class BaseApiContext {
    private static contexts: {context: APIRequestContext, key: BaseApiContext.ContextKey}[] = [];

    static async setupContextAsync(contextKey: BaseApiContext.ContextKey, options?) {
        const exsiting = this.contexts.find(x => {
            return x.key == contextKey
        });

        if (exsiting != null) {
            return;
        }

        const context = await request.newContext(options);
        const keyed = {
            context: context,
            key: contextKey
        };

        this.contexts.push(keyed);
    }

    static removeContext(contextKey: BaseApiContext.ContextKey) {
        this.contexts = this.contexts.filter(function(obj) {
            return obj.key != contextKey;
        });
    }

    static getContext(contextKey: BaseApiContext.ContextKey): APIRequestContext {
        console.debug(this.contexts)
        return this.contexts.find(x => {
            return x.key == contextKey;
        }).context;
    }
}

//Used to differentiate contexts.
//If you don't want to share context across requests then you can
//add new values here.
export namespace BaseApiContext {
    export enum ContextKey {
        Default
    }
}