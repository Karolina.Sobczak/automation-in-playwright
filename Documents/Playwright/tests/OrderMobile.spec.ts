import { NewSalePage } from '../Pages/NewSalePage';
import { OrderMobilePage } from '../Pages/OrderMobilePage';
import { ProductSummaryPage } from '../Pages/ProductSummaryPage';
import loginDetails from 'fixtures/loginDetails';
import test from '../componentsLibrary/baseTest';
import { LoginPage } from '../Pages/LoginPage';

test('Login + selecting Active Customer, selecting a product, customise the product', async ({ loginPage, orderMobilePage, newSalePage, productCustomisePage, productDeliveryPage }) => {
    test.setTimeout(30000);
    
    await loginPage.loginAsDefaultUser();
    await newSalePage.createNewProspectForExistingCustomer();
    await orderMobilePage.completeOrderMobileWithData();
    
    test.setTimeout(100000); // Adjust as needed
    await productCustomisePage.completeOrderMobileCustomisePage();
    await productDeliveryPage.completeDeliveryWithData();
});
