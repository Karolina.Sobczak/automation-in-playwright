/* import { test as baseTest } from "@playwright/test"
import { LoginPage } from "../Pages/LoginPage"
import { OrderMobilePage } from "../Pages/OrderMobilePage"
import { NewSalePage } from "../Pages/NewSalePage"
import { ProductCustomisePage } from "../Pages/ProductCustomisePage"
import { ProductDeliveryPage } from "../Pages/ProductDeliveryPage"
import { ProductSummaryPage } from "Pages/ProductSummaryPage"

 const test = baseTest.extend<{
  loginPage: LoginPage;
  orderMobilePage: OrderMobilePage;
  newSalePage: NewSalePage;
  productCustomisePage: ProductCustomisePage;
  productDeliveryPage: ProductDeliveryPage;
  productSummaryPage: ProductSummaryPage;

}>
({
  loginPage: async ({ page, context }, use) => {
    await use(new LoginPage(page, context));
  },
  orderMobilePage: async ({ page, context }, use) => {
    await use(new OrderMobilePage(page, context));
  },
  newSalePage: async ({ page }, use) => {
    await use(new NewSalePage(page));
  },
  productCustomisePage: async ({ page }, use) => {
    await use(new ProductCustomisePage(page));
  },
  productDeliveryPage: async ({ page }, use) => {
    await use(new ProductDeliveryPage(page));
  },
  productSummaryPage: async ({ page, context }, use) => {
    await use(new ProductSummaryPage(page));
  }
})


export default test;
*/
import { test as baseTest } from "@playwright/test";
import { LoginPage } from "../Pages/LoginPage";
import { OrderMobilePage } from "../Pages/OrderMobilePage";
import { NewSalePage } from "../Pages/NewSalePage";
import { ProductCustomisePage } from "../Pages/ProductCustomisePage";
import { ProductDeliveryPage } from "../Pages/ProductDeliveryPage"; // Adjusted to relative path
import { ProductSummaryPage } from "../Pages/ProductSummaryPage"; // Adjusted to relative path

const test = baseTest.extend<{
  loginPage: LoginPage;
  orderMobilePage: OrderMobilePage;
  newSalePage: NewSalePage;
  productCustomisePage: ProductCustomisePage;
  productDeliveryPage: ProductDeliveryPage;
  productSummaryPage: ProductSummaryPage;
}>({
  loginPage: async ({ page, context }, use) => {
    await use(new LoginPage(page, context));
  },
  orderMobilePage: async ({ page, context }, use) => {
    await use(new OrderMobilePage(page, context));
  },
  newSalePage: async ({ page }, use) => {
    await use(new NewSalePage(page));
  },
  productCustomisePage: async ({ page }, use) => {
    await use(new ProductCustomisePage(page));
  },
  productDeliveryPage: async ({ page }, use) => {
    await use(new ProductDeliveryPage(page));
  },
  productSummaryPage: async ({ page }, use) => {
    await use(new ProductSummaryPage(page)); // Check if context is needed here
  }
});

export default test;
