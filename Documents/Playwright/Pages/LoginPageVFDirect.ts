import { Page } from 'playwright';
import { LoginDetailsModel } from '../Models/LoginDetailsModel';

class LoginPageVFDirect {
    page: Page;
    loginDetails: LoginDetailsModel;

    constructor(page: Page) {
        this.page = page;
    }

    async navigate() {
        await this.page.goto('/');
    }

    async selectEnv(number: number) {
        await this.page.click('text="https://extusers.uat.aurora.io"');
        await this.page.click('text="https://pub-external-users-testenv' + number + '.uat.aurora.io"');
    }

    async enterUsername(username: string) {
        await this.page.click('[name="username"]');
        await this.page.type('[name="username"]', username);
    }

    async enterPassword(password: string) {
        await this.page.click('[name="password"]');
        await this.page.type('[name="password"]', password);
    }

    async clicklogInButton() {
        await this.page.click('text="Log in"');
    }

    async LogInto(envNumber: number, username: string, password: string) {
        console.log(envNumber, username, password);
        await this.navigate();
        await this.selectEnv(envNumber);
        await this.enterUsername(username);
        await this.enterPassword(password);
        await this.clicklogInButton();
    }

    async getLoginDetails() {
        const loginDetails = await this.page.fixture('loginDetailsVFDirect');
        this.loginDetails = loginDetails;
    }

    async loginWithValidCredentials() {
        await this.getLoginDetails();
        await this.LogInto(
            this.loginDetails.environmentNumber,
            this.loginDetails.username,
            this.loginDetails.password
        );
    }
}

export default LoginPageVFDirect;
