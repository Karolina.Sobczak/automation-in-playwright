import { test, expect } from '@playwright/test';

test.describe('API Testing', () => {
    test('should successfully retrieve user data', async ({ page }) => {
        await page.goto('https://giacom.haloitsm.com/');
        const response = await page.fetch('/users', { method: 'GET' });
        const responseBody = await response.json();
        
        expect(response.status()).toEqual(200);
        expect(responseBody).toHaveProperty('users');
    });
});
