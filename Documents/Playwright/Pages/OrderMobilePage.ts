﻿import { BrowserContext, Locator, Page } from 'playwright';
import loginDetails from 'fixtures/loginDetails';
import { expect } from '@playwright/test';


export class OrderMobilePage {
    readonly page: Page;
    context: BrowserContext;
    readonly mobileButton: Locator;
    readonly mobileButtonVFDirect: Locator;
    readonly productCatalogueButton: Locator;
    readonly mobileTariffs: Locator;
    readonly contractLengthButton: Locator;
    readonly contractLengthSelect: Locator;
    readonly quantityValueForProductMobile: Locator;
    readonly quantityDropdown: Locator;
    readonly quantityValueClick: Locator;
    readonly quantityValue: Locator;
    readonly boltOnButton: Locator;
    readonly boltOnData: Locator;
    readonly customiseButton: Locator;
    readonly searchProduct: Locator;
    readonly vodafoneFilterOn: Locator;
    readonly boltonTable: Locator;
    readonly awaitBoltOnLoad: Locator;
    readonly boltonSelectOption: Locator;
    readonly boltonAssertion: Locator;
    readonly optionToSelect: Locator;
    readonly customiseAssertion : Locator;

    constructor(page: Page, context: BrowserContext) {
        this.page = page;
        this.context = context;
        this.mobileButton = page.locator('//*[@data-cy="product-type-mobile"]');
        this.mobileButtonVFDirect = page.locator('[class="MuiTypography-root MuiTypography-body1 wrap47 css-19hocq9"]');
        this.productCatalogueButton = page.locator('[class="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeLarge MuiButton-containedSizeLarge MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeLarge MuiButton-containedSizeLarge css-azpgtc"]');
        this.mobileTariffs = page.locator('[class="MuiTypography-root MuiTypography-h2 css-1per694"]');
        this.contractLengthButton = page.getByLabel('Contract Length');
        this.contractLengthSelect = page.getByRole('option', { name: 'noMonths' }); 
        this.quantityDropdown = page.locator(`[@data-cy="${'productQty'}"] select`);
        this.quantityValueClick = page.locator('//*[contains(@class,"MuiNativeSelect-select MuiNativeSelect-outlined MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputSizeSmall css-7du94e")]').first();
        this.vodafoneFilterOn = page.getByLabel('Vodafone');
        this.awaitBoltOnLoad = page.locator('.MuiTableCell-root > div > .MuiFormControl-root > .MuiInputBase-root > .MuiSelect-select').first();
        this.boltonTable = page.getByTestId('BoltOnsTable');
        this.boltOnData = page.locator('.MuiTableCell-root > div > .MuiFormControl-root > .MuiInputBase-root > .MuiSelect-select').first();
        this.boltonSelectOption = page.locator('.MuiTableCell-root > div > .MuiFormControl-root > .MuiInputBase-root > .MuiSelect-select').first();
        this.optionToSelect = page.getByRole('option', { name: 'DCVFSD5 - Daisy Comms' });
        this.boltonAssertion = page.locator('//div[@class="MuiSelect-select MuiSelect-outlined MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputSizeSmall css-7a045g"]');
        this.customiseButton = page.locator('button').filter({ hasText: /^Customise$/ });
        this.searchProduct = page.locator('//*[contains(@placeholder,"Available")]'); 
        this.customiseAssertion = page.getByRole('button').filter({ hasText: /^Delivery$/ });

    }

    async noOfMonths (noMonths: string): Promise<Locator> {
        return this.page.locator(`//*[@role="option"][@data-value="${noMonths}"]`);
    }

    async selectMobileType() {
        await this.page.waitForLoadState('domcontentloaded');
        await this.mobileButton.click();
    }

    async selectMobileTypeVFDirect() {
        await this.mobileButtonVFDirect.click({timeout: 9000});
    }

    async clickProductCatalogueButton() {
        await this.page.waitForLoadState('domcontentloaded');
        await this.productCatalogueButton.click({timeout: 9000});
    }

    async clickContractLengthButton() {
        await this.contractLengthButton.click();

    }

    async clickContractLengthSelect(noMonths: string) {
        await (await this.noOfMonths(noMonths)).click({ timeout: 100000 });

    
    }

    async searchForProduct (productName: string) {
        await this.vodafoneFilterOn.check({timeout: 9000});
        await this.searchProduct.click();
        await this.searchProduct.fill(productName);

        }

    async selectQtyForProductMobile(productQty: string) { 
        
        await this.quantityValueClick.click();
        await this.quantityValueClick.selectOption(productQty);
    }

    async clickBoltOnButton() {
        await this.page.waitForLoadState('domcontentloaded');
        await this.page.waitForLoadState('networkidle');
     
        await expect(this.page.locator('.MuiTableCell-root > div > .MuiFormControl-root > .MuiInputBase-root > .MuiSelect-select').first()).toBeVisible({timeout: 9000});
        await this.boltonSelectOption.click();
        await this.optionToSelect.click();
    
    }
    
    async clickBoltOnData(boltOn: string) {

        await expect(this.page.locator('.MuiTableCell-root > div > .MuiFormControl-root > .MuiInputBase-root > .MuiSelect-select').first()).toBeVisible();
        await this.boltonSelectOption.click();
        await this.optionToSelect.click();
       
    }
    
    async clickCustomiseButton() {
        await this.customiseButton.click();
        await this.page.waitForLoadState('domcontentloaded');
        await this.page.waitForLoadState('networkidle');
        await expect(this.customiseAssertion).toBeVisible({ timeout: 10000 }); //ITS WORKING BUT ITS FLAKY

    }


     
    async completeOrderMobileVFDirect(noMonths, ) {
        await this.selectMobileTypeVFDirect();
        await this.clickProductCatalogueButton();
        await this.clickContractLengthButton();
        await this.clickContractLengthSelect(noMonths);
        await this.clickCustomiseButton();
    }
    async sessionContinues(){
            await this.context.storageState({path: 'storageState/auth_user.json'})
        }

    async completeOrderMobile(noMonths, productName, productQty, boltOn): Promise<void> {
        await this.selectMobileType();
        await this.clickProductCatalogueButton();
        await this.clickContractLengthButton();
        await this.clickContractLengthSelect(noMonths);
        await this.searchForProduct(productName);
        await this.selectQtyForProductMobile(productQty);
        await this.clickBoltOnButton();
        await this.clickBoltOnData(boltOn);
        await this.clickCustomiseButton();
        await this.sessionContinues();
    }

    async completeOrderMobileWithData(): Promise<void>{
    await this.completeOrderMobile(loginDetails.orderMobileDet.noMonths, 
        loginDetails.orderMobileDet.productName,
        loginDetails.orderMobileDet.productQty,loginDetails.orderMobileDet.boltOn
    );

    }
}





