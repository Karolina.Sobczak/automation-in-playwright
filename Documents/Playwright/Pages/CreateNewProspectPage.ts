import FakerHelper from '../../Helpers/FakerHelper';
import { Page } from 'playwright';

class CreateNewProspectPage {
    page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async clickCreateNewProspect() {
        await this.page.click('text="New Prospect"');
    }

    async typeEnterFirstNameInput(name: string) {
        await this.page.fill('[name="first_name"]', name);
    }

    async typeEnterLastNameInput(name: string) {
        await this.page.fill('[name="last_name"]', name);
    }

    async typeEnterEmailInput(name: string) {
        await this.page.fill('[name="email"]', name);
    }

    async typeEnterCompanyNameInput(name: string) {
        await this.page.fill('[name="account_name"]', name);
    }

    async typeOppNameInput(name: string) {
        await this.page.fill('[name="name"]', name);
    }

    async clickCreateProspectButton() {
        await this.page.click('text="Create new prospect and continue"');
    }

    async completeCreateNewProspect(firstName: string, lastName: string, email: string, companyName: string, oppName: string) {
        await this.clickCreateNewProspect();
        await this.typeEnterFirstNameInput(firstName);
        await this.typeEnterLastNameInput(lastName);
        await this.typeEnterEmailInput(email);
        await this.typeEnterCompanyNameInput(companyName);
        await this.typeOppNameInput(oppName);
        await this.clickCreateProspectButton();
    }

    async completeCreateNewProspectWithFakeData() {
        const fakerHelperObj = new FakerHelper();
        const randomUser = fakerHelperObj.createRandomUser();
        await this.completeCreateNewProspect(
            randomUser.ContactFirstName,
            randomUser.ContactLastName,
            randomUser.Email,
            randomUser.CompanyName,
            randomUser.AddAnOpportunityName
        );
    }
}

export default CreateNewProspectPage;
