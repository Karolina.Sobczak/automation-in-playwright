import { test, expect } from '@playwright/test';
import HaloPage from '../../Pages/NewSales/HaloPage';
import { HaloModel } from '../../Models/HaloModel';

test.describe('Login tests', () => {
    let haloDetailObj: HaloModel;
    const haloPageObj = new HaloPage();

    test.beforeEach(async () => {
        haloDetailObj = await import('../../fixtures/haloDetails.json') as HaloModel;
        await haloPageObj.navigate();
    });

    test('Valid login credentials', async ({ page }) => {
        await haloPageObj.enterUsername(haloDetailObj.username);
        await haloPageObj.enterPassword(haloDetailObj.password);
        await haloPageObj.clicklogInButton();

        expect(page.url()).toContain('/portal');
    });
});
