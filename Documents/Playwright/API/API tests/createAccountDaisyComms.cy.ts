import LoginActions from '../actions/LoginAction';
import { chromium } from 'playwright';

describe('API test for Create Prospect', async () => {
    let browser;
    const loginActions = new LoginActions();

    before(async () => {
        browser = await chromium.launch();
    });

    after(async () => {
        await browser.close();
    });

    it('Login to platform and create new prospect', async () => {
        const context = await browser.newContext();
        const page = await context.newPage();

        await loginActions.loginAsStandardUser();

        const token = loginActions.getJwtToken;
        console.log(token);

        await page.route('**/ExternalServices/v1/Accounts/Leads/Create', route => {
            route.request().continue({
                headers: {
                    ...route.request().headers(),
                    authorization: token
                }
            });
        });

        await page.goto('https://pub-kong-testenv08.uat.aurora.io/ExternalServices/v1/Accounts/Leads/Create', {
            method: 'POST',
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                background: '1',
                bank_account_name: 'BBBB',
                bank_account_number: '70872490',
                bank_account_sortcode: '404784',
                bank_name: 'AAA',
                billing_address_same: 1,
                billing_country: 'United Kingdom',
                birthdate: '1955-02-01',
                business_entity_type: 'Charity',
                company_building_number: 1,
                company_country: 'United Kingdom',
                company_country2: 'United Kingdom',
                company_country3: 'United Kingdom',
                company_locale: 'Horwich',
                company_post_town: 'Bolton',
                company_postcode: 'BL66DY',
                company_region: 'Lancashire',
                company_street: 'Telford Street',
                contract_type_id: 7,
                create_cost_centre_csv: 0,
                ebilling_email: 'karolina.sobczak@aurora.io',
                ebilling_enabled: 1,
                email: 'karolina.sobczak@aurora.io',
                first_name: 'User',
                idempotency_key: '539a2313d1c23873fa3f883169dcc1f2fc51d64d',
                large_print_bill: 0,
                last_name: 'UserA',
                lead: '69d00c2c-0bd5-ec36-032c-65f8031d3ad9',
                name: 'opp1',
                ok_to_email: 0,
                ok_to_mms: 0,
                ok_to_phone: 0,
                ok_to_post: 0,
                ok_to_sms: 0,
                order: 393,
                payment_type: 'DD',
                phone: '07894561230',
                salutation: 'Mr.'
            })
        });

        const response = await page.waitForResponse(response => response.url().includes('/ExternalServices/v1/Accounts/Leads/Create'));
        expect(response.status()).to.equal(200);

        const responseBody = await response.json();
        console.log('Response Body:', responseBody);
    });
});
