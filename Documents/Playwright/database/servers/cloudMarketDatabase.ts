import StringResult from "database/entities/string-result";
import connection from "../index"

export class CloudMarketDatabase {
    async getCloudMarketIdFromOrganisation_master(organisationId: string){
        const SqlQuery = `SELECT shared.binToGuid(CloudMarketId)
            FROM messageexchange.organisation_master
            WHERE organisationId = '${organisationId}'`
        
        const response = connection.query(SqlQuery);
        return response; 
    }

    async getListingIdFromCloudmarket_listings(serviceName: string){
        const SqlQuery = `SELECT shared.binToGuid(ListingId)
            FROM messageexchange.cloudmarket_listings
            WHERE name = '${serviceName}'`
        
        const response = connection.query(SqlQuery);
        return response; 
    }

    async getCustomerMexFromOrganisation_master(resellerMex: string, customerName: string){
        const SqlQuery = `SELECT OrganisationId
            FROM messageexchange.organisation_master
            WHERE PrimaryAccountId = '${resellerMex}'
            AND Name## = '${customerName}'`
        
        const response = connection.query(SqlQuery);
        return response; 
    }

    getResellerMexFromUsers(resellerEmail: string): Promise<string> {
        return new Promise((resolve, reject) => {
            connection.query<StringResult[]>(
                `SELECT primaryaccount_id AS 'value'
                 FROM messageexchange.users
                 WHERE user_emailaddress = ?`,
              [resellerEmail],
              (err, rows) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(rows[0].value);
                }
              }
            );
          });
    }
}