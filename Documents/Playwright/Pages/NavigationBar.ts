import { Page } from 'playwright';

class NavigationBar {
    page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async clickDashboardButton() {
        await this.page.click('[data-cy="SubNav"] >> text=Dashboard');
    }

    async clickNewSaledButton() {
        await this.page.click('[data-cy="SubNav"] >> text=New Sale');
    }

    async clickSalesStatusButton() {
        await this.page.click('[data-cy="SubNav"] >> text=Sales Status');
    }

    async clickProspectsButton() {
        await this.page.click('[data-cy="SubNav"] >> text=Prospects');
    }
}

export default NavigationBar;
