/* import LoginActionVFDirect from '../../e2e/APIs/Actions/LoginActionVFDirect';
import { LoginDetailsModel } from '../Models/LoginDetailsModel';
import LoginPage from '../Pages/LoginPage';
import { chromium } from 'playwright';

class SessionHelper {
    readonly loginActionObj: LoginActionVFDirect;
    readonly loginPageObj: LoginPage;
    loginDetails: LoginDetailsModel;

    constructor() {
        this.loginActionObj = new LoginActionVFDirect();
        this.loginPageObj = new LoginPage();
        this.loginDetails = {} as LoginDetailsModel; // Initialize loginDetails
    }

    async loginViaApiAndStoreContext(username: string, password: string, permissionNumber: string): Promise<void> {
        const authToken = await this.loginActionObj.login(username, password, permissionNumber);
        localStorage.setItem('authToken', authToken);
    }

    async loginAsStandardUserViaApiAndStoreContext(): Promise<void> {
        const loginDetails = await import('../../../cypress/fixtures/loginDetailsVFDirect.json');
        this.loginDetails = loginDetails;

        await this.loginViaApiAndStoreContext(
            this.loginDetails.username,
            this.loginDetails.password,
            this.loginDetails.permissionNumber
        );
    }

    async loginAndStoreContext(permissionNumber: string, username: string, password: string): Promise<void> {
        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        await page.goto('URL_TO_LOGIN_PAGE');
        await page.fill('#username', username);
        await page.fill('#password', password);
        await page.click('button[type="submit"]');

        await page.waitForNavigation({ url: 'EXPECTED_URL_AFTER_LOGIN' });
        
        localStorage.setItem('authToken', 'VALUE_FROM_AUTH_TOKEN');

        await browser.close();
    }

    async loginAsStandardUserAndStoreContext(): Promise<void> {
        const loginDetails = await import('../../../cypress/fixtures/loginDetailsVFDirect.json');
        this.loginDetails = loginDetails;

        await this.loginAndStoreContext(
            this.loginDetails.environmentNumber,
            this.loginDetails.username,
            this.loginDetails.password
        );
    }
}

export default SessionHelper;
