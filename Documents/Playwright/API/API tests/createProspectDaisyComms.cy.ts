import LoginActions from '../actions/LoginAction';
import { chromium } from 'playwright';

describe('API test for Create Prospect', () => {
    let browser;
    const loginActions = new LoginActions();

    before(async () => {
        browser = await chromium.launch();
    });

    after(async () => {
        await browser.close();
    });

    it('Login to platform and create new prospect', async () => {
        const context = await browser.newContext();
        const page = await context.newPage();

        await loginActions.loginAsStandardUser();

        const token = loginActions.getJwtToken;
        console.log(token);

        await page.route('**/ExternalServices/v1/Accounts/Leads/Create', route => {
            route.request().continue({
                headers: {
                    ...route.request().headers(),
                    authorization: token
                }
            });
        });

        const response = await page.goto('https://pub-kong-testenv08.uat.aurora.io/ExternalServices/v1/Accounts/Leads/Create', {
            method: 'POST',
            headers: {
                authorization: token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                first_name: 'UserAPItest',
                last_name: 'LastNameAPItest',
                email: 'karolina.sobczak@aurora.io',
                name: 'testAPIcypress',
                account_name: 'CypressAPItest'
            })
        });

        expect(response.status()).to.equal(200);

        const responseBody = await response.json();
        console.log('Response Body:', responseBody);
    });
});
