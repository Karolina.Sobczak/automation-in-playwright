import HaloController from '../Controllers/HaloController';
import { HaloModel } from '../../Models/HaloModel';

class LoginToHalo {
    readonly haloPageObj: HaloController;
    readonly loginToHaloObj: ExternalServicesController;
    haloModelForAPI: HaloModel;

    constructor() {
        this.haloPageObj = new HaloController();
        this.loginToHaloObj = new ExternalServicesController();
        this.haloModelForAPI = new HaloModel();
    }

    async loginToHaloAPI(username: string, password: string, authCode: string): Promise<void> {
        const base64AuthCode = 'Basic ' + Buffer.from(username + ":" + password).toString('base64');

        const response = await this.haloPageObj.login(base64AuthCode);
        const loginAuthCode = 'Bearer ' + response.body.refresh_token;

        const jwtResponse = await this.haloPageObj.jwt(loginAuthCode);
        this.setJwtToken = 'Bearer ' + jwtResponse.body.jwt;
    }
}

export default LoginToHalo;
