import { HaloModel } from '../../Models/HaloModel';
import { Page } from 'playwright';

class HaloPage {
    page: Page;
    haloDetails: HaloModel;

    constructor(page: Page) {
        this.page = page;
    }

    async navigate() {
        await this.page.goto('https://giacom.haloitsm.com/portal/auth');
    }

    async enterUsername(username: string) {
        const usernameInput = await this.page.waitForSelector('[name="Username"]');
        await usernameInput.click();
        await usernameInput.type(username);
    }

    async enterPassword(password: string) {
        const passwordInput = await this.page.waitForSelector('[name="Password"]');
        await passwordInput.click();
        await passwordInput.type(password);
    }

    async clicklogInButton() {
        const loginButton = await this.page.waitForSelector('.login-btn');
        await loginButton.click();
    }

    async LogInto(username: string, password: string) {
        await this.navigate();
        await this.enterUsername(username);
        await this.enterPassword(password);
        await this.clicklogInButton();
    }

    async getLoginDetails() {
        // Assuming you have a method to retrieve login details asynchronously
        return await this.page.waitForFile('haloDetails.json');
    }

    async loginWithValidCredentials() {
        const haloDetails = await this.getLoginDetails();
        this.haloDetails = haloDetails;

        await this.LogInto(
            this.haloDetails.username,
            this.haloDetails.password
        );
    }
}

export default HaloPage;
