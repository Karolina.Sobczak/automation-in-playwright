import  LoginController from '../controllers/LoginController';
import { LoginDetailsModel } from '../../Models/LoginDetailsModel';

export class LoginAction {
    readonly loginPageObj: LoginController;
    loginDetails: LoginDetailsModel;
    private jwtToken: string;

    constructor() {
        this.loginPageObj = new LoginController();
        this.loginDetails = new LoginDetailsModel();
        this.jwtToken = '';
    }

    public set setJwtToken(val: string) {
        this.jwtToken = val;
    }

    public get getJwtToken(): string {
        console.log(this.jwtToken);
        return this.jwtToken;
    }

    async login(username: string, password: string, permissionNumber: string): Promise<void> {
        const authCode = 'Basic ' + Buffer.from(username + ':' + password).toString('base64');

        const response = await this.loginPageObj.login(permissionNumber, authCode);
        const loginAuthCode = 'Bearer ' + response.body.refresh_token;
        
        const jwtResponse = await this.loginPageObj.jwt(loginAuthCode);
        this.setJwtToken = 'Bearer ' + jwtResponse.body.jwt;
    }

    async loginAsStandardUser(): Promise<void> {
        this.loginDetails = await this.loginPageObj.getLoginDetails(); // Assuming there's a method to retrieve login details

        await this.login(this.loginDetails.username, this.loginDetails.password, this.loginDetails.permissionNumber);
    }
}

export default LoginAction;
