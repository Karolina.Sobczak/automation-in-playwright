import fs from "fs";
import type { Page } from "@playwright/test";
import { BrowserContext, expect } from "@playwright/test";

const authFile = "storageState/auth_user.json";

export class WebActions {
  readonly page: Page;
  readonly context: BrowserContext;

  constructor(page: Page, context: BrowserContext) {
    this.page = page;
    this.context = context;
  }

  async delay(time: number): Promise<void> {
    return new Promise(function (resolve) {
      setTimeout(resolve, time);
    });
  }

  async clickByText(text: string): Promise<void> {
    await this.page.getByText(text, { exact: true }).click(); //Matches locator with exact text and clicks
  }

  async clickElementJS(locator: string): Promise<void> {
    await this.page.$eval(locator, (element: HTMLElement) => element.click());
  }

  async readValuesFromTextFile(filePath: string): Promise<string> {
    return fs.readFileSync(`${filePath}`, `utf-8`);
  }

  async writeDataIntoTextFile(
    filePath: number | fs.PathLike,
    data: string | NodeJS.ArrayBufferView
  ): Promise<void> {
    fs.writeFile(filePath, data, (error) => {
      if (error) throw error;
    });
  }

  async clearCookies(): Promise<void> {
    await this.context.clearCookies();
  }

  async clearAuthFile(): Promise<void> {
    var jsonData = JSON.stringify({
        "cookies": [],
        "origins": []
      })
    
      fs.writeFile(authFile, jsonData, function(err) {
        if (err) {
            console.log(err);
        }
      });
  }

  async closePage(): Promise<void> {
    await this.page.close();
  }
}
