import { LoginTestModel } from '../../Models/loginTestModel';
import { Page } from 'playwright';

class LoginTestPage {
    page: Page;
    loginTestDetails: LoginTestModel;

    constructor(page: Page) {
        this.page = page;
    }

    async navigate() {
        await this.page.goto('https://dws.cascadecloud.co.uk/index.aspx');
    }

    async enterUserNameCasc(username: string) {
        const userNameInput = await this.page.waitForSelector('[placeholder="Your username"]');
        await userNameInput.click();
        await userNameInput.type(username);
    }

    async clickButtonNextCasc() {
        const buttonNext = await this.page.waitForSelector('[type="submit"][value="login"]');
        await buttonNext.click();
    }

    async getLoginDetails() {
        // Assuming you have a method to retrieve login details asynchronously
        return await this.page.waitForFile('loginTestDetails.json');
    }

    async LoginTestCredentials(username: string) {
        const loginTestDetails = await this.getLoginDetails();
        this.loginTestDetails = loginTestDetails;
        await this.enterUserNameCasc(username);
        await this.clickButtonNextCasc();
    }
}

export default LoginTestPage;
