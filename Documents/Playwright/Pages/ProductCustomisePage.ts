import { th } from '@faker-js/faker';
import { BrowserContext, Locator, Page } from 'playwright';
import { expect } from 'playwright/test';

export class ProductCustomisePage {
    readonly page: Page;
    context: BrowserContext;
    readonly bbConfig: Locator;
    readonly appSelect: Locator;
    readonly appointmentTimeDropDown: Locator;
    readonly firstAppointmentTimeListItem: Locator;
    readonly contactNameField: Locator;
    readonly editConfigButton: Locator;
    readonly partnerRefNo: Locator;
    readonly barsdropdown: Locator;
    readonly barcombinedSMS: Locator;
    readonly barcombinedSMSchecked: Locator;
    readonly oneOff: Locator; 
    readonly monthlyCredit: Locator;
    readonly mobileBillCap: Locator;
    readonly billLimitAmount: Locator;
    readonly predispatchedSIM: Locator;
    readonly yesPredispatchedSIM: Locator;
    readonly billLimitCheckbox: Locator;
    readonly deliveryButton: Locator;
    readonly returnToCustomise: Locator;
    readonly deleteButton: Locator;
    readonly durationAirtimeCredit: Locator;
    readonly airtimecredit1Month: Locator;
 


    constructor(page: Page) {
     //   this.page = page;
      //  this.context = context;
        this.bbConfig = page.locator('[data-cy="wlrBroadbandEditConfig"]');
        this.appSelect = page.locator('[data-cy="wlrAppointmentSelect"]');
        this.appointmentTimeDropDown = page.locator('[data-cy="wlrAppointmentSelect"]');
        this.firstAppointmentTimeListItem = page.locator('[data-value="0"]');
        this.contactNameField = page.locator('[name="contact_name"]');
        this.editConfigButton = page.getByRole('button', { name: 'Edit' }); 
        this.partnerRefNo = page.getByLabel('Unique Reference Number*');
        this.barsdropdown = page.locator('div:nth-child(2) > div:nth-child(4) > .MuiPaper-root > .MuiCardHeader-root > .MuiCardHeader-action > .MuiBox-root > .MuiButtonBase-root');
        this.barcombinedSMS = page.getByText('Combined SMS');
        this.barcombinedSMSchecked = page.getByLabel('Combined SMS');
        this.oneOff = page.getByLabel('One Off Credit Amount');
        this.monthlyCredit = page.getByLabel('Monthly Credit Amount');
        this.billLimitCheckbox = page.locator('//*[@id="root"]/div[4]/div/div[2]/div/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div/div[2]/div/div/div/div/div[1]/label/span[1]/input');
        this.billLimitAmount = page.getByTestId('bill_limit');
      // ITS POINTING ON THE ELEMENT BUT STILL DOESNT WORK this.predispatchedSIM = page.locator('div').filter({ hasText: /^Pre Dispatched SIM\?No$/ }).getByTestId('ArrowDropDownIcon');
        this.predispatchedSIM = page.locator('.MuiSelect-nativeInput css-1k3x8v3');
        this.yesPredispatchedSIM = page.getByRole('option', { name: /Yes/i });
        this.mobileBillCap = page.locator('//*[@id="root"]/div[4]/div/div[2]/div/div/div[3]/div[2]/div/div/div[2]/div/div[2]/label/span[1]');
        this.deliveryButton = page.locator('button').filter({ hasText: /^Delivery$/ });
        this.returnToCustomise = page.getByRole('button', { name: 'Return to Customise' });
        this.deleteButton = page.getByRole('button', { name: 'Delete' });
        this.durationAirtimeCredit = page.getByLabel('Number of Months');
        this.airtimecredit1Month = page.getByRole('option', { name: '1 Month' });
   
    }

    async clickLinesCallsAndBroadbandButton() {
        await this.bbConfig.click();
    }

    async clickAppointmentTimeDropDown() {
        await this.appointmentTimeDropDown.click();
    }

    async selectFirstAppointmentTimeListItem() {
        await this.firstAppointmentTimeListItem.click();
    }

    async typeInContactNameField(name: string) {
        await this.page.waitForSelector('[name="contact_name"]');
        await this.contactNameField.fill(name);
    }

    async typeInContactTelephoneField(number: string) {
        await this.page.waitForSelector('[name="contact_telephone"]');
        await this.page.fill('[name="contact_telephone"]', number);
    }

    async clickReserveNowButton() {
        await this.page.waitForSelector('[data-cy="NumberReservationButton"]');
        await this.page.click('[data-cy="NumberReservationButton"]');
    }

  /*  async typeInReferenceNumberField(number: string) {
        await this.page.waitForSelector('[name="partner_reference_number"]');
        await this.page.fill('[name="partner_reference_number"]', number);
    }
*/  
    async billLimitCheck() {
        await this.billLimitCheckbox.click();
        await expect(this.billLimitCheckbox.isChecked());

    }
    async clickDeliveryButton() {
        await this.page.waitForSelector('[data-cy="StepNavFooterNext"]');
        await this.page.click('[data-cy="StepNavFooterNext"]');
    }

    async clickMobileEditButton() {
        await this.editConfigButton.scrollIntoViewIfNeeded();
        await this.editConfigButton.click();
   
    }

    async typeUniqueRefNo() {
        await this.partnerRefNo.click();
        await this.partnerRefNo.fill('121');
    }

    async barsAdd() {
        await this.barsdropdown.click();
        await this.barcombinedSMS.click();
        expect(await this.barcombinedSMSchecked.isChecked()).toBeTruthy();
    }
    
    async airtimecredit() {
        await this.oneOff.click();
        await this.oneOff.fill('11');
        await this.monthlyCredit.click();
        await this.monthlyCredit.fill('5');
        await this.durationAirtimeCredit.click();
        await this.airtimecredit1Month.click();
        await this.barsdropdown.click();

    }

    async billLimitAdd() {    
        await this.billLimitAmount.click();
        await this.billLimitAmount.fill('10');
        await this.billLimitCheckbox.click();
        expect(await this.billLimitCheckbox.isChecked()).toBeTruthy();
    }
    
    async returnToCustomiseClick() {
        await this.returnToCustomise.click({ force: true });
          
        }
    
    async clickCheckboxMobileBillCap() {
        // await this.editConfigButton.scrollIntoViewIfNeeded();
     //   await this.mobileBillCap.scrollIntoViewIfNeeded(); //page.getByTestId('CheckBoxIcon')
        await this.mobileBillCap.click();
        await this.mobileBillCap.setChecked(true);
        
    } 

   /* async typeProvideSimSerial() {
        await this.predispatchedSIM.click();
        await this.yesPredispatchedSIM.selectOption({value: '1' });
        await this.page.fill('[name="sim_buffer_serial"]', '89441000304153915224');
    } THIS PART DOESN'T WORK*/

    async moveToDeliveryPage() {
        await this.deliveryButton.click();
    }

    async completeTheProductCustomisePage(contactName: string, contactNumber: string, referenceNumber: string) {
        await this.clickLinesCallsAndBroadbandButton();
        await this.clickAppointmentTimeDropDown();
        await this.selectFirstAppointmentTimeListItem();
        await this.typeInContactNameField(contactName);
        await this.typeInContactTelephoneField(contactNumber);
        await this.clickReserveNowButton();
        await this.typeUniqueRefNo();
        await this.page.waitForTimeout(20000);
        await this.moveToDeliveryPage();
    }

    async completeOrderMobileCustomisePage(): Promise<void> {
        await this.clickMobileEditButton();
        await this.typeUniqueRefNo();
        await this.barsAdd();
        await this.airtimecredit();
        await this.returnToCustomiseClick();
        await this.clickCheckboxMobileBillCap();
        await this.moveToDeliveryPage();
    }

    async completeOrderMobileCustomisePageVFDirect(): Promise<void> {
        await this.clickMobileEditButton();
        await this.page.waitForTimeout(20000);
        await this.clickDeliveryButton();
    }
}

