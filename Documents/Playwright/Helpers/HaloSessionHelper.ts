import HaloAction from '../../e2e/APIs/Actions/HaloAction';
import { HaloModel } from '../Models/HaloModel';
import HaloPage from '../Pages/NewSales/HaloPage';
import { chromium, devices } from 'playwright';

class HaloSessionHelper {
    readonly haloActionObj: HaloAction;
    readonly haloPageObj: HaloPage;
    haloDetails: HaloModel;

    constructor() {
        this.haloActionObj = new HaloAction();
        this.haloPageObj = new HaloPage();
        this.haloDetails = {} as HaloModel; // Initialize haloDetails
    }

    async loginViaApiAndStoreContextHalo(username: string, password: string): Promise<void> {
        const authToken = await this.haloActionObj.login(username, password);
        localStorage.setItem('authToken', authToken);
    }

    async loginAsStandardUserViaApiAndStoreContextHalo(): Promise<void> {
        const haloDetails = await import('../../../cypress/fixtures/haloDetails.json');
        this.haloDetails = haloDetails;

        await this.loginViaApiAndStoreContextHalo(this.haloDetails.username, this.haloDetails.password);
    }

    async loginAndStoreContextHalo(username: string, password: string): Promise<void> {
        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        await page.goto('URL_TO_HALO_LOGIN_PAGE');
        await page.fill('#username', username);
        await page.fill('#password', password);
        await page.click('button[type="submit"]');

        await page.waitForNavigation({ url: 'EXPECTED_URL_AFTER_LOGIN' });
        
        localStorage.setItem('authToken', 'VALUE_FROM_AUTH_TOKEN');

        await browser.close();
    }

    async loginAsStandardUserAndStoreContextHalo(): Promise<void> {
        const haloDetails = await import('../../../cypress/fixtures/haloDetails.json');
        this.haloDetails = haloDetails;

        await this.loginAndStoreContextHalo(this.haloDetails.username, this.haloDetails.password);
    }
}

export default HaloSessionHelper;
