import { RowDataPacket } from "mysql2";

export default interface StringResult extends RowDataPacket {
    value?: string;
}