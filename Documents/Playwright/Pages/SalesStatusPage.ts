import { Page } from 'playwright';

class SalesStatusPage {
    page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async navigate() {
        await this.page.goto('sales/sales-status');
    }

    async clickOpportunitiesCard() {
        await this.page.click('span:has-text("Opportunities")');
    }

    async assertOnFilteredOption(name: string) {
        await this.page.waitForSelector('[data-cy="added-filter-status"]');
        const filteredOption = await this.page.$eval('[data-cy="added-filter-status"]', el => el.textContent.trim());
        expect(filteredOption).toContain(name);
    }
}

export default SalesStatusPage;
