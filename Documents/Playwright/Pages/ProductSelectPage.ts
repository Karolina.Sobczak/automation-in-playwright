import { Page } from 'playwright';

class ProductSelectPage {
    page: Page;

    constructor(page: Page) {
        this.page = page;
    }

    async clickLinesCallsAndBroadbandButton() {
        await this.page.waitForSelector('button', { text: 'Lines, Calls & Broadband', timeout: 10000 });
        await this.page.click('button', { text: 'Lines, Calls & Broadband' });
    }

    async clickProductCatalogueButton() {
        await this.page.click('[data-cy="StepNavFooterNext"]');
    }

    async clickContractLengthDropDown() {
        await this.page.click('[aria-labelledby="contractLengthSelectField"]');
    }

    async selectContractLengthInMonths(noMonths: any) {
        await this.page.click('li[data-value="' + noMonths + '"]');
    }

    async selectInstallType(typeName: any) {
        await this.page.click('[data-cy="' + typeName + '"]');
    }

    async typePostcode(postcode: any) {
        await this.page.fill('[name="postcode"]', postcode);
    }

    async clickChooseAddressButton() {
        await this.page.click('[data-cy="AddressPickerButton"]');
    }

    async selectAnAddress(address: any) {
        await this.page.click('text="' + address + '"');
    }

    async clickFindProductButton() {
        await this.page.click('button', { text: 'Find Products' });
    }

    async selectQtyForProduct(productName: any, productQty: any) {
        await this.page.selectOption('[data-cy="' + productName + '"] select', productQty);
    }

    async clickCustomiseButton() {
        await this.page.click('[data-cy="StepNavFooterNext"]');
    }

    async completeTheProductSelectPage(contractLengthInMonths: any, installTypeName: any, findPostcode: any, selectAddress: any, productName: any, productQty: any) {
        await this.clickLinesCallsAndBroadbandButton();
        await this.clickProductCatalogueButton();
        await this.clickContractLengthDropDown();
        await this.selectContractLengthInMonths(contractLengthInMonths);
        await this.selectInstallType(installTypeName);
        await this.typePostcode(findPostcode);
        await this.clickChooseAddressButton();
        await this.selectAnAddress(selectAddress);
        await this.clickFindProductButton();
        await this.selectQtyForProduct(productName, productQty);
        await this.clickCustomiseButton();
    }
}

export default ProductSelectPage;
