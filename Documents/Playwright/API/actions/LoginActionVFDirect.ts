import { LoginController } from '../Controllers/LoginController';
import { LoginDetailsModel } from '../../Models/LoginDetailsModel';

class LoginActionVFDirect {
    readonly loginPageObj: LoginController;
    loginDetails: LoginDetailsModel;

    constructor() {
        this.loginPageObj = new LoginController();
        this.loginDetails = new LoginDetailsModel();
    }

    async login(username: string, password: string, permissionNumber: string): Promise<void> {
        const authCode = 'Basic ' + Buffer.from(username + ':' + password).toString('base64');

        const response = await this.loginPageObj.login(permissionNumber, authCode);
        const loginAuthCode = 'Bearer ' + response.body.refresh_token;
        
        await this.loginPageObj.jwt(loginAuthCode);
    }

    async loginAsStandardUser(): Promise<void> {
        this.loginDetails = await this.loginPageObj.getLoginDetailsVFDirect(); // Assuming there's a method to retrieve login details

        await this.login(this.loginDetails.username, this.loginDetails.password, this.loginDetails.permissionNumber);
    }
}

export default LoginActionVFDirect;
