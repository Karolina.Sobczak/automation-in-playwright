import HaloController from '../Controllers/HaloController';
import { HaloModel } from '../../Models/HaloModel';
class HaloAction {
    readonly loginPageObj = new HaloController();
    haloDetails: HaloModel;
    public jwtToken: string
    public set setJwtToken(val) {
        this.jwtToken = val;
    }
  
    login(username, password) {
        const authCode = 'Basic ' + btoa(username + ":" + password);
        return this.loginPageObj.login(authCode).then((response) => {
            const loginAuthCode = 'Bearer ' + response.body.refresh_token

            this.loginPageObj.jwt(loginAuthCode).then(response => {
                this.setJwtToken = 'Bearer ' + response.body.jwt;
            });
        })
    }
    loginAsStandardUser() {
        return cy.fixture('haloDetails').then(haloDetails => {
            this.haloDetails = haloDetails;
            this.login(this.haloDetails.username, this.haloDetails.password)
        })
    }
}
export default HaloAction;