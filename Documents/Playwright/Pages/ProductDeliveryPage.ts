import loginDetails from 'fixtures/loginDetails';
import { BrowserContext, Locator, Page } from 'playwright';
import { expect } from 'playwright/test';


export class ProductDeliveryPage {
    readonly page: Page;
    context: BrowserContext;
    readonly deliveryTextAssertion: Locator;
    readonly trackingDetail: Locator;
    readonly newcontactButton: Locator;
    readonly nameContact: Locator;
    readonly lastnameContact: Locator;
    readonly emailContact: Locator;
    readonly contactTypeDropdown: Locator;
    readonly contractSignatoryContact: Locator;
    readonly postcode: Locator;
    readonly chooseAddressButton: Locator;
    readonly addressSelection: Locator;
    readonly createContactButton: Locator;
    readonly contactaddedAssertion: Locator;
    readonly trackingdetails: Locator;
    readonly selectContact: Locator;
    readonly selectNewContact: Locator;
    readonly summaryButton: Locator;
    readonly confirmButton: Locator;
    readonly summarypageAssertion: Locator;

    constructor(page: Page) {
        this.page = page;
        this.deliveryTextAssertion = page.getByText('Please fill out the delivery information');
        this.trackingDetail = page.getByLabel('Email or Mobile Number');
        this.newcontactButton = page.locator('div').filter({ hasText: /^Add new Contact$/ }).getByRole('button');
        this.nameContact = page.getByLabel('First Name *');
        this.lastnameContact = page.getByLabel('Last Name *');
        this.emailContact = page.getByLabel('Email *');
        this.contactTypeDropdown = page.getByLabel('Add new Contact').locator('div').filter({ hasText: 'Contact Type *' }).nth(3);
        this.contractSignatoryContact = page.getByRole('option', { name: 'Contract Signatory' });
        this.postcode = page.getByLabel('Postcode');
        this.chooseAddressButton = page.getByRole('button', { name: 'Choose Address' });
        this.addressSelection = page.getByRole('button', { name: '1 Telford Street, Horwich, Bolton, BL66DY', exact: true });
        this.createContactButton = page.getByRole('button', { name: 'Create Contact' });
        this.contactaddedAssertion = page.getByRole('heading', { name: 'Contact added' });
        this.trackingdetails = page.getByLabel('Email or Mobile Number');
        this.selectContact = page.getByRole('button', { name: '​', exact: true });
        this.selectNewContact = page.locator('.MuiButtonBase-root > .MuiListItemText-root > span').first();
        this.summaryButton = page.locator('button').filter({ hasText: /^Summary$/ });
        this.confirmButton = page.getByRole('button', { name: 'Confirm' });
        this.summarypageAssertion = page.getByRole('heading', { name: 'Summary' });
    }

    async assertDeliveryPage() {
        await this.deliveryTextAssertion.isVisible();
    }

    async typeTrackingDetails(email: string) {
        await this.trackingDetail.click();
        await this.trackingDetail.fill(email);
    }

    async clickAddNewContactButton(name: string, lastName: string, email: string, postcode: string) {
        await this.newcontactButton.click();
        await this.nameContact.click();
        await this.nameContact.fill(name);
        await this.lastnameContact.click();
        await this.lastnameContact.fill(lastName);
        await this.emailContact.click();
        await this.emailContact.fill(email);
        await this.contactTypeDropdown.click();
        await this.contractSignatoryContact.click();
        await this.postcode.click();
        await this.postcode.fill(postcode);
        await this.chooseAddressButton.click();
        await this.addressSelection.click();
        await this.createContactButton.click();
        await this.contactaddedAssertion.isVisible();
        await this.trackingdetails.click();
        await this.trackingdetails.fill(email);
        await this.selectContact.click();
        await this.selectNewContact.click();
        await this.summaryButton.click();
        await this.confirmButton.click();
        await this. summarypageAssertion.isVisible();
       

    }

    

    async selectTestAddress() {
        await this.page.click('[class="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-root MuiButton-contained MuiButton-containedSecondary MuiButton-sizeMedium MuiButton-containedSizeMedium css-3d9p2j"]');
    }

    async clickSummaryButton() {
        await this.page.click('[class="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeLarge MuiButton-containedSizeLarge MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeLarge MuiButton-containedSizeLarge css-azpgtc"], [data-cy="StepNavFooterNext"]');
    }

    async clickConfirmButton() {
        await this.page.click('[class="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeMedium MuiButton-containedSizeMedium css-15dhsee"], [data-cy="FinalStepConfirmButton"]:nth-child(3)');
    }

    async clickVfDirectAddNewContactButton() {
        await this.page.click('[class="MuiBox-root css-1nylpq2"]');
    }

    async clickCreateContactVFdirect() {
        await this.page.click('[type="submit"]');
    }

   /* async CreateNewContactVFDirect() {
        const newContactDetails = await this.getNewContactDetails();
        this.newContactDetails = newContactDetails;
        console.log('newContactDetailsAre' + this.newContactDetails);
        await this.assertDeliveryAssertion();
        await this.typeTrackingDetails();
        await this.clickVfDirectAddNewContactButton();
        await this.typeFirstNameInput(this.newContactDetails.firstName);
        await this.typeLastNameInput(this.newContactDetails.lastName);
        await this.typeEmailInput(this.newContactDetails.email);
        await this.typePostcodeInput(this.newContactDetails.postCode);
        await this.clickContactTypeButton();
        await this.selectContactTypeSelect();
        await this.selectTestAddress();
        await this.selectSelectAddressFromTheList();
        await this.clickCreateContactVFdirect();
        await this.page.waitForTimeout(10000);
        await this.clickSummaryButton();
        await this.clickConfirmButton();
    }


    async completeOrderDeliveryPage() {
        await this.storingDeliveryPagePrice();
        await this.CreateNewContact();
    }*/

   /* async completeOrderDeliveryPageVFDirect() {
        await this.storingDeliveryPagePrice();
        await this.CreateNewContactVFDirect();
    } */

    async storingDeliveryPagePrice() {
        await this.page.waitForSelector('[data-cy="price-Per-Month"]');
        const deliveryPrice = await this.page.textContent('[data-cy="price-Per-Month"]');
        console.log('Delivery Price: ' + deliveryPrice);
    }

    

    async completeDeliveryPage(email, name, lastName, postcode): Promise<void> {
        await this.assertDeliveryPage();
        await this.typeTrackingDetails(email);
        await this.clickAddNewContactButton(name, lastName, email, postcode);
     
    }
    async completeDeliveryWithData(): Promise<void>{
        await this.completeDeliveryPage(loginDetails.contactDet.email, loginDetails.contactDet.name, loginDetails.contactDet.lastName, loginDetails.contactDet.postcode);
        
}

}
