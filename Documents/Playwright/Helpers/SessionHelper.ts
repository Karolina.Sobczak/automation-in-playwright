/*import   LoginAction  from '../API/actions/LoginAction';
//import { LoginDetailsModel } from '../Models/LoginDetailsModel';
import { LoginPage } from '../Pages/LoginPage';
import { chromium } from 'playwright';

class SessionHelper {
     const loginAction = new LoginAction();
     const loginPageObj: LoginPage;
     const loginDetails: LoginDetailsModel;

    constructor() {
        this.loginActionObj = new LoginAction();
        this.loginPageObj = new LoginPage();
        this.loginDetails = {} as LoginDetailsModel; // Initialize loginDetails
    }

    async loginViaApiAndStoreContext(username: string, password: string, permissionNumber: string): Promise<void> {
        const authToken = await this.loginActionObj.login(username, password, permissionNumber);
        localStorage.setItem('authToken', authToken);
    }

    async loginAsStandardUserViaApiAndStoreContext(): Promise<void> {
        const loginDetails = await import('../fixtures/loginDetails');
        this.loginDetails = loginDetails;

        await this.loginViaApiAndStoreContext(
            this.loginDetails.username,
            this.loginDetails.password,
            this.loginDetails.permissionNumber
        );
    }

    async loginAndStoreContext(permissionNumber: string, username: string, password: string): Promise<void> {
        const browser = await chromium.launch();
        const context = await browser.newContext();
        const page = await context.newPage();

        await page.goto('URL_TO_LOGIN_PAGE');
        await page.fill('#username', username);
        await page.fill('#password', password);
        await page.click('button[type="submit"]');

        await page.waitForNavigation({ url: 'EXPECTED_URL_AFTER_LOGIN' });
        
        localStorage.setItem('authToken', 'VALUE_FROM_AUTH_TOKEN');

        await browser.close();
    }

    async loginAsStandardUserAndStoreContext(): Promise<void> {
        const loginDetails = await import('../fixtures/loginDetails');
        this.loginDetails = loginDetails;

        await this.loginAndStoreContext(
            this.loginDetails.environmentNumber,
            this.loginDetails.username,
            this.loginDetails.password
        );
    }
}
export default SessionHelper;
/*
export class SessionHelper {
    readonly page: Page;
    context: BrowserContext;
    readonly mobileButton: Locator;
  

    constructor(page: Page, context: BrowserContext) {
        this.page = page;
        this.context = context;
        this.mobileButton = page.locator('//*[@data-cy="product-type-mobile"]');
   

    }
*/
import LoginAction from '../API/actions/LoginAction';
import { LoginPage } from '../Pages/LoginPage';
import { chromium, Browser, BrowserContext, Page } from 'playwright';

class SessionHelper {
    readonly loginActionObj: LoginAction;
    readonly loginPageObj: LoginPage;
    readonly loginDetails: any; // Use appropriate type
    

    constructor(page: Page, context: BrowserContext ) {
        this.loginActionObj = new LoginAction();
        this.loginPageObj = new LoginPage(page,context );
        this.loginDetails = {}; // Initialize loginDetails
    }

    async loginViaApiAndStoreContext(username: string, password: string, permissionNumber: string): Promise<void> {
        const authToken = await this.loginActionObj.login(username, password, permissionNumber);
        // Note: In Playwright, you don't have direct access to localStorage like in the browser
        // You might need to set it via page.evaluate later
        console.log(`Auth token: ${authToken}`); // Store the token as needed
    }

    async loginAsStandardUserViaApiAndStoreContext(): Promise<void> {
        const loginDetails = await import('../fixtures/loginDetails');
        this.loginDetails = loginDetails;

        await this.loginViaApiAndStoreContext(
            this.loginDetails.username,
            this.loginDetails.password,
            this.loginDetails.permissionNumber
        );
    }

    async loginAndStoreContext(page: Page, permissionNumber: string, username: string, password: string): Promise<void> {
        await page.goto('URL_TO_LOGIN_PAGE');
        await page.fill('#username', username);
        await page.fill('#password', password);
        await page.click('button[type="submit"]');

        await page.waitForNavigation({ url: 'EXPECTED_URL_AFTER_LOGIN' });
        
        // Set the auth token in local storage
        await page.evaluate((token) => {
            localStorage.setItem('authToken', token);
        }, 'VALUE_FROM_AUTH_TOKEN'); // Replace with the actual token if available
    }

    async loginAsStandardUserAndStoreContext(): Promise<void> {
        const loginDetails = await import('../fixtures/loginDetails');
        this.loginDetails = loginDetails;

        const browser: Browser = await chromium.launch();
        const context: BrowserContext = await browser.newContext();
        const page: Page = await context.newPage();

        await this.loginAndStoreContext(page, this.loginDetails.permissionNumber, this.loginDetails.username, this.loginDetails.password);

        await browser.close();
    }
}

export default SessionHelper;
