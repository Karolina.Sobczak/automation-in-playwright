import { test, expect } from '@playwright/test';
import SessionHelper from '../../Helpers/SessionHelper';
import NewSalePage from '../../Pages/NewSales/NewSalePage';
import ProductSelectPage from '../../Pages/NewSales/ProductSelectPage';
import ProductCustomisePage from '../../Pages/NewSales/ProductCustomisePage';
import ProductDeliveryPage from '../../Pages/NewSales/ProductDeliveryPage';
import { NewContactDetailsModel } from '../../Models/NewContactDetailsModel';

test.describe('Order New Line product', () => {
    const sessionHelperObj = new SessionHelper();
    const newSalePageObj = new NewSalePage();
    const productSelectPageObj = new ProductSelectPage();
    const productCustomisePageObj = new ProductCustomisePage();
    const productDeliveryPageObj = new ProductDeliveryPage();
    let newContactDetailsObj: NewContactDetailsModel;

    test.beforeEach(async () => {
        await sessionHelperObj.loginAsStandardUserAndStoreContext();
        await newSalePageObj.navigate();
    });

    test('Order New Line product', async () => {
        await newSalePageObj.createNewProspectForExisitingCustomer('CyressTest1');
        await productSelectPageObj.completeTheProductSelectPage(
            '1', // contractLengthInMonths
            'New Line', // installTypeName
            'PL312DU', // findPostcode
            '3, Castle Glade, Castle Street, Bodmin, PL312DU', // selectAddress
            'WLR Single Line', // productName
            '1' // productQty
        );
        await productCustomisePageObj.completeTheProductCustomisePage(
            'John', // contactName
            '07888888888', // contactNumber
            '98779887' // referenceNumber
        );
        
        await productDeliveryPageObj.completeOrderDeliveryPage();
        newContactDetailsObj = await import('../../fixtures/newContactDetails.json') as NewContactDetailsModel;
        await productDeliveryPageObj.typeFirstNameInput(newContactDetailsObj.firstName);
        await productDeliveryPageObj.typeLastNameInput(newContactDetailsObj.lastName);
        await productDeliveryPageObj.typeEmailInput(newContactDetailsObj.email);
        await productDeliveryPageObj.typePostcodeInput(newContactDetailsObj.postCode);
    });
});
