import { test, expect } from '@playwright/test';
import SessionHelper from '../Helpers/SessionHelper';
import SalesStatusPage from '../Pages/SalesStatusPage';

test.describe('within the sales status page', () => {
    const sessionHelperObj = new SessionHelper();
    const salesStatusPageObj = new SalesStatusPage();

    test.beforeEach(async () => {
        await sessionHelperObj.loginAsStandardUserAndStoreContext();
        await salesStatusPageObj.navigate();
    });

    test('click on the Opportunities card', async () => {
        await salesStatusPageObj.clickOpportunitiesCard();
        await salesStatusPageObj.assertOnFilteredOption('Status - Opportunity');
    });
});
